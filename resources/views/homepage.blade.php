@extends('layouts.app')

@section('headerScripts')
    <link href="{{ asset('css/cover.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <img id="logo" src="{{ asset('images/logo.png') }}" alt="Star Wars">

                <star-wars-qeries></star-wars-qeries>
                
            </div>
        </div>
    </div>
@endsection

@section('footerScripts')
@endsection
