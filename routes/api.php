<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')
Route::get('/longest-opening-crawal', 'API\LongestOpeningCrawalController@longestOpeningCrawal');

Route::get('/most-appeared-character', 'API\MostAppearedCharacterController@mostAppearedCharacter');


Route::get('/species-appeared-in-the-most-number', 'API\SpeciesAppearedInTheMostNumberController@speciesAppearedInTheMostNumber');

Route::get('/planet-longest-number-vehicle-pilots', 'API\PlanetLongestNumberVehiclePilotsController@planetLongestNumberVehiclePilots');
