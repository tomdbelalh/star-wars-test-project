<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Starship extends Model
{
    public function films () {
    	return $this->hasMany('App\Film');
    }

    public function pilots () {
    	return $this->hasMany('App\People');
    }

    
}
