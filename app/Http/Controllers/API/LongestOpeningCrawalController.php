<?php

namespace App\Http\Controllers\API;

use App\Film;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class LongestOpeningCrawalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function longestOpeningCrawal()
    {
        $result = [];
        $largestCharacterFilmId = DB::table('films_characters')
                ->select(DB::raw('film_id, count(people_id) as totalCharacters'))
                ->groupBy('film_id')
                ->orderBy('totalCharacters', 'DESC')
                ->first()->film_id;

        $filmTitle = Film::where('id', $largestCharacterFilmId)->first()->title;
        $result[] = ['name' => $filmTitle];

        return response()->json($result); //'Return of the Jodi';
    }
}
