<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Species;
use DB;
use Illuminate\Http\Request;

class SpeciesAppearedInTheMostNumberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function speciesAppearedInTheMostNumber()
    {
        $result = [];
        $resultArr = [];
        $speciesIds = [];
        $maxAppearedSpeciesOccurance = 0;

        $speciesAppearedRecords = DB::table('films_species')
                ->select(DB::raw('species_id, count(*) as totalAppeared'))
                ->groupBy('species_id')
                ->orderBy('totalAppeared', 'DESC')
                ->get(['species_id', 'totalAppeared']);
        $maxAppearedSpeciesOccurance = $speciesAppearedRecords[0]->totalAppeared;

        foreach($speciesAppearedRecords as $record) {
            if($record->totalAppeared == $maxAppearedSpeciesOccurance) 
                $speciesIds[] = $record->species_id;
            else
                break;
        }
        
        $speciesNames = Species::whereIn('id', $speciesIds)->get(['id', 'name']);
        foreach($speciesNames as $record) {
            $result[$record->id]['name'] = $record->name;
            $result[$record->id]['peopleActedNumber'] = 0;
        }

        $peopleActedSpeciesRecords = DB::table('species_people')
                ->select(DB::raw('species_id, count(*) as totalPeopleActedThisCharacter'))
                ->whereIn('species_id', $speciesIds)
                ->groupBy('species_id')
                ->orderBy('totalPeopleActedThisCharacter', 'DESC')
                ->get(['species_id', 'totalPeopleActedThisCharacter']);

        foreach($peopleActedSpeciesRecords as $record) {
            $result[$record->species_id]['peopleActedNumber'] = $record->totalPeopleActedThisCharacter;
        }

        foreach($result as $r) {
            $resultArr[] = ['name' => $r['name'] . ' (' . $r['peopleActedNumber'] . ')'];
        }

        return response()->json($resultArr); //'Return of the Jodi';
    }
}
