<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\People;
use DB;
use Illuminate\Http\Request;

class MostAppearedCharacterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function mostAppearedCharacter()
    {
        // $result = [];
        $peopleIds = [];
        $maxAppearedPeopleOccurance = 0;

        $characterAppearedRecords = DB::table('films_characters')
                ->select(DB::raw('people_id, count(*) as totalAppeared'))
                ->groupBy('people_id')
                ->orderBy('totalAppeared', 'DESC')
                ->get(['people_id', 'totalAppeared']);
        $maxAppearedPeopleOccurance = $characterAppearedRecords[0]->totalAppeared;

        foreach($characterAppearedRecords as $record) {
            if($record->totalAppeared == $maxAppearedPeopleOccurance) 
                $peopleIds[] = $record->people_id;
            else
                break;
        }


        $characterNames = People::whereIn('id', $peopleIds)->get(['name'])->toArray();

        return response()->json($characterNames); //'Return of the Jodi';
    }
}
