<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Species;
use DB;
use Illuminate\Http\Request;

class PlanetLongestNumberVehiclePilotsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function planetLongestNumberVehiclePilots()
    {
        $result = [];
        $resultArr = [];
        $filmIds = [];
        $maxAppearedVehicleOccurance = 0;

        $vehiclesAppearedRecords = DB::table('films_vehicles')
                ->select(DB::raw('film_id, count(*) as totalAppeared'))
                ->groupBy('film_id')
                ->orderBy('totalAppeared', 'DESC')
                ->get(['film_id', 'totalAppeared']);
        $maxAppearedVehicleOccurance = $vehiclesAppearedRecords[0]->totalAppeared;

        foreach($vehiclesAppearedRecords as $record) {
            if($record->totalAppeared == $maxAppearedVehicleOccurance) 
                $filmIds[] = $record->film_id;
            else
                break;
        }
        $filmIdsCSV = implode(",", $filmIds);

        // dd($filmIds);
        
        $speciesNames = DB::select("SELECT pl.name AS planetName, ppl.name AS peopleName, spec.name AS specifyName       
                        FROM planets AS pl   
                        INNER JOIN films_planets AS fp ON pl.id = fp.planet_id 
                        INNER JOIN films_vehicles AS fv ON fp.film_id = fv.film_id 
                        INNER JOIN vehicles_pilots AS vp ON vp.vehicle_id = fv.vehicle_id 
                        INNER JOIN people AS ppl ON ppl.id = vp.people_id  
                        INNER JOIN species_people AS sppl ON sppl.people_id = ppl.id 
                        INNER JOIN species AS spec ON spec.id = sppl.species_id 
                        WHERE fv.film_id IN ($filmIdsCSV)");
        dd($speciesNames);

        // $speciesNames = Species::whereIn('id', $filmIds)->get(['id', 'name']);
        // foreach($speciesNames as $record) {
        //     $result[$record->id]['name'] = $record->name;
        //     $result[$record->id]['peopleActedNumber'] = 0;
        // }

        // $peopleActedSpeciesRecords = DB::table('species_people')
        //         ->select(DB::raw('species_id, count(*) as totalPeopleActedThisCharacter'))
        //         ->whereIn('species_id', $speciesIds)
        //         ->groupBy('species_id')
        //         ->orderBy('totalPeopleActedThisCharacter', 'DESC')
        //         ->get(['species_id', 'totalPeopleActedThisCharacter']);

        // foreach($peopleActedSpeciesRecords as $record) {
        //     $result[$record->species_id]['peopleActedNumber'] = $record->totalPeopleActedThisCharacter;
        // }

        // foreach($result as $r) {
        //     $resultArr[] = ['name' => $r['name'] . ' (' . $r['peopleActedNumber'] . ')'];
        // }

        return response()->json($resultArr); //'Return of the Jodi';
    }
}
