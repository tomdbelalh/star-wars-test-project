<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Species extends Model
{
    public function films () {
    	return $this->belongsToMany('App\Film', 'films_species');
    }

    public function people () {
    	return $this->belongsToMany('App\People', 'species_people');
    }
}
