<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    public function characters () {
    	return $this->belongsToMany('App\People', 'films_characters');
    }

    public function planets () {
    	return $this->belongsToMany('App\Planet', 'films_planets');
    }
    
    public function species () {
    	return $this->belongsToMany('App\Species', 'films_species');
    }
    
    public function starships () {
    	return $this->belongsToMany('App\Starship', 'films_starships');
    }
    
    public function vehicles () {
    	return $this->belongsToMany('App\Vehicle', 'films_vehicles');
    }
    
    
}
